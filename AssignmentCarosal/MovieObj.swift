//
//  MovieObj.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import UIKit

class MovieObj: NSObject {
    var ID:Int = 0
    var mediaType:String = "N/A"
    var title:String = "N/A"
    var poster_path:String = "N/A"
    var overview:String = "N/A"
    var release_date:String = "N/A"
    
    private override init() {
        super.init()
    }
    
    init(resultSet:[String:Any]) {
        super.init()
        if let id = resultSet["id"] as? Int
        {
            self.ID = id
        }
        if let media_type = resultSet["media_type"] as? String
        {
            self.mediaType = media_type
        }
        if let title = resultSet["title"] as? String
        {
            self.title = title
        }
        if let poster_path = resultSet["poster_path"] as? String
        {
            self.poster_path = poster_path
        }
        if let overview = resultSet["overview"] as? String
        {
            self.overview = overview
        }
        if let release_date = resultSet["release_date"] as? String
        {
            self.release_date = release_date
        }
     
    }
}

