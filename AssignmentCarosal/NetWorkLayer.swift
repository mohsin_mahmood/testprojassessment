//
//  NetWorkLayer.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import Alamofire

class NetWorkLayer: NSObject {

    private var request:DataRequest?
    let serverUrl = "https://api.themoviedb.org/3/search/multi"
    public func getMovies(searchString query:String, responseBlock: @escaping (Error?,[String:Any]?) -> Void)
    {
        
        let dict: [String:Any] = ["api_key":"3d0cda4466f269e793e9283f6ce0b75e",
                                  "language":"en-US",
                                  "query" : query,
                                  "page" : "1",
                                  "includeadult" : "false"]
        request =  Alamofire.request("\(serverUrl)", method: .get, parameters:dict).responseJSON
            { (response) in
                
                if response.result.isSuccess
                {
                    do
                    {
                        
                        let dict:[String:Any] = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:Any]
                        responseBlock(nil,dict)
                    }
                    catch
                    {
                        responseBlock(error,nil)
                    }
                }
                else
                {
                    //server issue
                    responseBlock(response.result.error,nil)
                }
        }
    }
    
    func cancelReq()
    {
        request?.cancel()
        request = nil
    }

}
    


