//
//  AssignmentCarosal.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import UIKit

public class MainAssignmentCarosal: NSObject {
    public override init() {
        super.init()
    }
    
    public func getInitialViewcontroller(NumberOfCarosal noCsl:Int, SearchTitlesArray stArr:[String]) -> UIViewController
    {
        assert(noCsl == stArr.count,"Titles and number of Carosals must be equal")
        let s = UIStoryboard(name: "AssignmentCarosal", bundle: Bundle(for: CarosalMoviesViewController.self))
        let vc = s.instantiateInitialViewController() as! UINavigationController
        let ctrlTop = vc.topViewController as! CarosalMoviesViewController
        ctrlTop.arrayOfTitles = stArr.sorted()
        ctrlTop.numberOfCarosals = noCsl
        return vc
    }
}
