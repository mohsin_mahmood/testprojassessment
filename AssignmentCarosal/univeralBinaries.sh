FRAMEWORK='AssignmentCarosal'
BUILD=build
FRAMEWORK_PATH=$FRAMEWORK.framework
# iOS
rm -Rf $BUILD
xcodebuild archive -workspace $FRAMEWORK.xcworkspace -scheme $FRAMEWORK -sdk iphoneos
xcodebuild build -workspace $FRAMEWORK.xcworkspace -scheme $FRAMEWORK -sdk iphonesimulator
cp -RL $BUILD/Release-iphoneos $BUILD/Release-universal
cp -RL $BUILD/Release-iphonesimulator/$FRAMEWORK_PATH/Modules/$FRAMEWORK.swiftmodule/* $BUILD/Release-universal/$FRAMEWORK_PATH/Modules/$FRAMEWORK.swiftmodule
lipo -create $BUILD/Release-iphoneos/$FRAMEWORK_PATH/$FRAMEWORK $BUILD/Release-iphonesimulator/$FRAMEWORK_PATH/$FRAMEWORK -output $BUILD/Release-universal/$FRAMEWORK_PATH/$FRAMEWORK
