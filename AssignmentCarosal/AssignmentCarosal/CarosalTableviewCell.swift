//
//  CarosalTableviewCell.swift
//  TestProject
//
//  Created by Mohsin Mahmood on 4/2/18.
//  Copyright © 2018 Mohsin Mahmood. All rights reserved.
//

import UIKit

class CarosalTableviewCell: UITableViewCell {
    @IBOutlet var carosalView:Carosal!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateCell(queryStr:String, parentCtrl:CarosalMoviesViewController)
    {
        carosalView.loadCarosalData(query: queryStr,parentCtrl:parentCtrl)
    }

}
