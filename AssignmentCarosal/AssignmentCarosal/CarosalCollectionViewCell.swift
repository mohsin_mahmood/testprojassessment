//
//  CarosalCollectionViewCell.swift
//  TestProject
//
//  Created by Mohsin Mahmood on 4/2/18.
//  Copyright © 2018 Mohsin Mahmood. All rights reserved.
//

import UIKit

class CarosalCollectionViewCell: UICollectionViewCell {
    static let baseURL = "https://image.tmdb.org/t/p/w185"
    
    @IBOutlet var imageView:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populateCellImage(imagePath:String)
    {
        imageView.showImageWithURLStr(urlStr: "\(CarosalCollectionViewCell.baseURL)\(imagePath)")
    }
}
