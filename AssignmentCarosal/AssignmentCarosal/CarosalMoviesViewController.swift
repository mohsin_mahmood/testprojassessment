//
//  ViewController.swift
//  TestProject
//
//  Created by Mohsin Mahmood on 4/2/18.
//  Copyright © 2018 Mohsin Mahmood. All rights reserved.
//

import UIKit

class CarosalMoviesViewController: UIViewController {

    var numberOfCarosals:Int = 0
    var arrayOfTitles:[String] = [String]()
    @IBOutlet var carosalTableView:UITableViewDataSource!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let cellReuseIdentifier =  "carosalCell"

}

extension CarosalMoviesViewController:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfCarosals
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Carosal.carosalHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CarosalTableviewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CarosalTableviewCell
        cell.populateCell(queryStr: arrayOfTitles[indexPath.row],parentCtrl: self)
        return cell
    }
}
//DetailMovie
extension CarosalMoviesViewController
{
    func performSegueWithObject(movObj:MovieObj)
    {
        self.performSegue(withIdentifier: "DetailMovie", sender: movObj)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let ctrl = segue.destination as! DetailViewController
        ctrl.movieObject = sender as? MovieObj
    }
}
