//
//  DetailViewController.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var releaseDateLbl:UILabel!
    @IBOutlet var descLbl:UILabel!
    @IBOutlet var titleLbl:UILabel!
    
    var movieObject:MovieObj?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLbl.text = movieObject?.title
        descLbl.text = movieObject?.overview
        releaseDateLbl.text = movieObject?.release_date
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
