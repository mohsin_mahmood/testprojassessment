//
//  CarosalView.swift
//  TestProject
//
//  Created by Mohsin Mahmood on 4/2/18.
//  Copyright © 2018 Mohsin Mahmood. All rights reserved.
//

import UIKit

class CarosalView: UIView {
    @IBOutlet  var searchTxt: UITextField!
    @IBOutlet  var carosalCollectionView: UICollectionView!
    weak var parent:CarosalMoviesViewController?
    private var networkObj:NetWorkLayer?
    let cellReuseIdentifier =  "myCollectionCell"
    var resultModels = [MovieObj]()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadCarosalData(searchQuery searchTxt:String,parentCtrl:CarosalMoviesViewController)
    {
        let nib = UINib(nibName: "CarosalCollectionViewCell", bundle: Bundle(for:MainAssignmentCarosal.self))
        self.carosalCollectionView.register(nib, forCellWithReuseIdentifier: cellReuseIdentifier)
        
        networkObj = NetWorkLayer()
        networkObj?.cancelReq()
        networkObj!.getMovies(searchString: searchTxt) { (err, resp) in
            
            if err == nil
            {
                self.resultModels =  MovieResponseParser().parseResult(resultSet: resp!)
            }
            else
            {
                self.resultModels =  [MovieObj]()
            }
            self.carosalCollectionView.reloadData()
        }
        DispatchQueue.main.async {
          self.searchTxt.text = searchTxt
        }
        self.parent = parentCtrl
    }
    
    //Removing possibilities of init other then xib or storyboard
    private override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    
}

//MARK - Delegates
extension CarosalView:UICollectionViewDelegate,UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resultModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        parent?.performSegueWithObject(movObj: resultModels[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell:CarosalCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CarosalCollectionViewCell
        cell.populateCellImage(imagePath: resultModels[indexPath.row].poster_path)
    
        return cell
    }
}
