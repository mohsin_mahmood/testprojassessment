//
//  Carosal.swift
//  TestProject
//
//  Created by Mohsin Mahmood on 4/2/18.
//  Copyright © 2018 Mohsin Mahmood. All rights reserved.
//

/* Class Needs To use in storyBoard Only */


import UIKit

class Carosal: UIView {
    static var carosalHeight:CGFloat =  183
    private var carosalView:CarosalView?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        carosalView = Bundle(for: MainAssignmentCarosal.self).loadNibNamed("CarosalView", owner: self, options: nil)!.first as? CarosalView
        carosalView?.frame = self.bounds
        carosalView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(carosalView!)
    }
    
    func loadCarosalData(query:String,parentCtrl:CarosalMoviesViewController)
    {
       carosalView!.loadCarosalData(searchQuery: query,parentCtrl: parentCtrl)
    }
    //Removing possibilities of init other then xib or storyboard
    private override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    private convenience init() {
        self.init(frame: CGRect.zero)
    }
    
}
