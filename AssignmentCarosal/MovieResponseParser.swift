//
//  MovieResponseParser.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import UIKit

class MovieResponseParser: NSObject {

    override init() {
        super.init()
    }
    
    public func parseResult(resultSet:[String:Any]) -> [MovieObj] {
        var movieObjects = [MovieObj]()
        if let innerDict = resultSet["results"] as? [AnyObject]
        {
            var model:MovieObj?
            for dict in innerDict
            {
                model = MovieObj.init(resultSet: dict as! [String : Any])
                movieObjects.append(model!)
            }
        }
        return movieObjects
    }
}
