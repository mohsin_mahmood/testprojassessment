//
//  Extensions.swift
//  AssignmentCarosal
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import SDWebImage
extension UIImageView {
    
    func showImageWithURLStr(urlStr:String)
    {
        self.sd_setShowActivityIndicatorView(true)
        self.sd_setIndicatorStyle(.gray)
        self.sd_setImage(with: URL(string:urlStr)!,completed:{ (image, err, ii, r) in
            if let imageAct = image
            {
                self.image = imageAct
            }
        })
    }
}
