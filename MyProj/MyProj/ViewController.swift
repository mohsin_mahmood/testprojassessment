//
//  ViewController.swift
//  MyProj
//
//  Created by Mohsin Mahmood on 4/4/18.
//  Copyright © 2018 StarzPlay. All rights reserved.
//

import UIKit
import AssignmentCarosal
import Alamofire
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let ctrl = MainAssignmentCarosal().getInitialViewcontroller(NumberOfCarosal: 5, SearchTitlesArray: ["Action","Comedy","Adventure","Drama","Sci fi"])
        self.present(ctrl, animated: true, completion: nil)
    }

    
    
}

